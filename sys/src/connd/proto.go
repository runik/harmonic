package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type ActionHandler struct {
	cm *ConnectionManager
}

func MakeActionHandler(cm *ConnectionManager) ActionHandler {
	return ActionHandler{cm}
}

func (ah *ActionHandler) Run(line string) string {
	parts := strings.Split(line, " ")

	action := parts[0]
	args := parts[1:]

	switch action {
	case "start":
		return ah.start(args)
	case "stop":
		return ah.stop(args)
	case "reset":
		return ah.reset()
	case "list":
		return ah.list()
	default:
		return "Bad command"
	}
}

func (ah *ActionHandler) start(args []string) string {
	commtype := args[0]
	cmdline := strings.Join(args[1:], " ")

	cid := ah.cm.MakeEndpoint(cmdline, commtype)
	if cid > 0 {
		fmt.Println("[+] Started new endpoint", cid, "with `"+cmdline+"`")
	} else {
		fmt.Fprintf(os.Stderr, "[-] Failed to start endpoint process with `"+cmdline+"`")
	}
	return fmt.Sprintf("%d", cid)
}

func (ah *ActionHandler) stop(args []string) string {
	if len(args) == 0 {
		return "-1"
	}
	cid, err := strconv.Atoi(args[0])
	if err != nil {
		return "-1"
	}

	if cid < 1 {
		return "-1"
	}

	return fmt.Sprintf("%d", ah.cm.StopEndpoint(cid))
}

func (ah *ActionHandler) list() string {
	fmt.Println("[+] Issuing listing")
	ah.scanSockets()
	listing := ah.cm.List()
	out := ""
	if len(listing) == 0 {
		return "No endpoints"
	}
	for e := range listing {
		if listing[e].Index == 0 {
			continue
		}
		out += fmt.Sprintf("%d [%d]:\t"+listing[e].Cmdline+"\t"+listing[e].Comment+"\n", listing[e].Index, listing[e].Pid)
	}

	return out
}

func (ah *ActionHandler) reset() string {
	return fmt.Sprintf("%d", ah.cm.ResetEndpoints())
}

func (ah *ActionHandler) scanSockets() {
	headers := ah.cm.List()
	for i := range headers {
		if headers[i].Comms == CONT {
			// The connection is continuous so
			// has a socket FD
			cmd := exec.Command("lsof", fmt.Sprintf("-p%d", headers[i].Pid))
			output, err := cmd.CombinedOutput()
			if err != nil {
				continue
			}

			lines := strings.Split(string(output), "\n")

			for l := range lines {
				ah.scanLine(lines[l], headers[i].Index)
			}
		}
	}
}

func (ah *ActionHandler) scanLine(line string, cid int) {
	if strings.Contains(line, "IPv") {
		if strings.Contains(line, "LISTEN") {
			ah.cm.UpdateComment(cid, "Listening")
		} else if strings.Contains(line, "ESTABLISHED") {
			fields := strings.Fields(line)
			rstr := strings.Split(fields[8], ">")
			parts := strings.Split(rstr[1], ":")
			ah.cm.UpdateComment(cid, fmt.Sprintf("Established -> "+parts[0]))
		}
	}
}
