package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("<harmonic> connd")
	cm := NewConnectionManager()
	d, err := NewDaemon(cm)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[-] Error Starting daemon:", err)
		return
	}
	d.Start()
}
