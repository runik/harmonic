package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"syscall"
)

// The type of endpoint so it can be handled
// in a different way. The reason why continuous
// is not referred to as a stream is because it
// is a higher level abstraction. Both discrete
// and continuous can be an underlying TCP stream
// but discrete communication is made up of single
// request/responses (HTTP) while a continuous
// endpoint could be a dedicated TCP stream (netcat)
const (
	CONT = iota // A continuous stream of data
	DISC = iota // Discrete communications
)

type PipePair struct {
	in          *bufio.Reader
	out         *bufio.Writer
	fdin, fdout *os.File
}

func makePipePair(in *bufio.Reader, out *bufio.Writer, fdin *os.File, fdout *os.File) PipePair {
	return PipePair{in, out, fdin, fdout}
}

// A connection wraps the process driving
// an endpoint
type Connection struct {
	Index          int            // Index ID assigned to the connection
	Command        *exec.Cmd      // The command object
	Cmdline        string         // Original command line used
	Stdout, Stderr io.ReadCloser  // Pipes for process stderr and stdout
	Stdin          io.WriteCloser // Pipes for process stdin
	Pipes          PipePair       // A pair of file system FIFO pipes
	Comms          int            // Type of communication (CONT|DISC)
	Comment        string         // The Comment attached to the socket
}

// Represents data that is useful outside
// of the connection manager and connection
type ConnectionHeader struct {
	Index   int
	Pid     int
	Comms   int
	Cmdline string
	Comment string
}

// Read from the stdout of the connection
// endpoint and write to the output pipe
func (c *Connection) ReadStream() {
	buf := make([]byte, 1024)
	for {
		n, err := c.Stdout.Read(buf)
		if n > 0 {
			c.Pipes.out.Write(buf[:n])
			c.Pipes.out.Flush()
			if err == io.EOF {
				break
			}
		}

		if err != nil {
			break
		}
	}
}

// Read from the stderr of the connection
// endpoint and write to the output pipe
func (c *Connection) ReadError() {
	buf := make([]byte, 1024)
	for {
		n, err := c.Stderr.Read(buf)
		if n > 0 {
			c.Pipes.out.Write(buf[:n])
			c.Pipes.out.Flush()
			if err == io.EOF {
				break
			}
		}

		if err != nil {
			break
		}
	}
}

// Read from the input pipe and write
// to the stdin of the connection endpoint
func (c *Connection) WriteStream() {
	buf := make([]byte, 1024)
	for {
		n, err := c.Pipes.in.Read(buf)
		if n > 0 {
			c.Stdin.Write(buf[:n])
			if err == io.EOF {
				break
			}
		}

		if err != nil {
			break
		}
	}
}

// Stop the process driving the connection
func (c *Connection) Stop() {
	c.Command.Process.Kill()
}

// Construct a header from the current connection
func (c *Connection) Header() ConnectionHeader {
	return ConnectionHeader{
		Index:   c.Index,
		Pid:     c.Command.Process.Pid,
		Comms:   c.Comms,
		Cmdline: c.Cmdline,
		Comment: c.Comment,
	}
}

// The connection manager handles all the
// connection endpoints
type ConnectionManager struct {
	active []*Connection
	cid    int
}

// Construct a new connection with given index, command and arguments
func NewConnection(index int, comms int, cmdstr string, argstr ...string) *Connection {
	cmd := exec.Command(cmdstr, argstr...)
	si, _ := cmd.StdinPipe()
	so, _ := cmd.StdoutPipe()
	se, _ := cmd.StderrPipe()

	c := new(Connection)
	c.Index = index
	c.Command = cmd
	c.Cmdline = cmdstr + " " + strings.Join(argstr, " ")
	c.Stdout = so
	c.Stdin = si
	c.Stderr = se
	c.Pipes = makePipePair(nil, nil, nil, nil)
	c.Comms = comms
	c.Comment = ""
	return c
}

// Create a new connection endpoint in the manager
func (cm *ConnectionManager) MakeEndpoint(cmdstr string, commtype string) int {
	parts := strings.Split(cmdstr, " ")
	comms := CONT
	if commtype == "d" || commtype == "disc" {
		comms = DISC
	}
	c := NewConnection(cm.cid, comms, parts[0], parts[1:]...)

	err := c.Command.Start()
	if err != nil {
		log.Println(err)
		return -1
	}

	err = cm.assignPipes(c)

	if err != nil {
		log.Println(err)
		return -1
	}
	cid := cm.cid
	cm.cid++

	// Start the R/W routines
	go c.ReadStream()
	go c.ReadError()
	go c.WriteStream()

	// Start process termination handler
	go cm.Wait(c)
	cm.active = append(cm.active, c)
	return cid
}

// Stop the end point with given id
func (cm *ConnectionManager) StopEndpoint(cid int) int {
	for i := range cm.active {
		if cm.active[i].Index == cid {
			fmt.Println("[+] Stopping endpoint ", cid)
			cm.active[i].Stop()
			return cid
		}
	}
	return -1
}

// Stop all the endpoints and reset the endpoint index
// id back to zero
func (cm *ConnectionManager) ResetEndpoints() int {
	fmt.Println("[+] Issuing reset")
	for i := range cm.active {
		cid := cm.active[i].Index
		fmt.Println("[+] Stopping endpoint ", cid)
		cm.active[i].Stop()
	}
	cm.cid = 1
	fmt.Println("[+] Reset done")
	return 0
}

// Get the header of an endpoint with given id.
// If the header does not exist it just returns an
// invalid header with -1 index and PID
func (cm *ConnectionManager) GetHeader(cid int) ConnectionHeader {
	for i := range cm.active {
		h := cm.active[i].Header()
		if h.Index == cid {
			return h
		}
	}
	return ConnectionHeader{-1, -1, -1, "", ""}
}

// Construct an array of active connection headers
// to find a listing of actice connections
func (cm *ConnectionManager) List() []ConnectionHeader {
	headers := make([]ConnectionHeader, len(cm.active))
	for i := range cm.active {
		headers = append(headers, cm.active[i].Header())
	}
	return headers
}

// Update the comment attached to a connection
func (cm *ConnectionManager) UpdateComment(cid int, comment string) {
	for i := range cm.active {
		if cm.active[i].Index == cid {
			cm.active[i].Comment = comment
		}
	}
}

// Assign IO pipes to a connection
func (cm *ConnectionManager) assignPipes(c *Connection) error {
	ipath := pipePath(c.Index, "i")
	opath := pipePath(c.Index, "o")
	syscall.Mkfifo(ipath, 0666)
	syscall.Mkfifo(opath, 0666)

	if err := os.Chmod(ipath, 0666); err != nil {
		return err
	}

	if err := os.Chmod(opath, 0666); err != nil {
		return err
	}

	fdin, err := os.OpenFile(ipath, syscall.O_RDWR, 0666)
	if err != nil {
		return err
	}

	fdout, err := os.OpenFile(opath, syscall.O_RDWR, 0666)
	if err != nil {
		return err
	}

	pipein := bufio.NewReader(fdin)
	pipeout := bufio.NewWriter(fdout)

	c.Pipes = makePipePair(pipein, pipeout, fdin, fdout)
	return nil
}

// Routine to wait for the end of a process and
// once triggeded, clean up and notify any clients
func (cm *ConnectionManager) Wait(c *Connection) {
	c.Command.Wait()

	c.Pipes.out.Write([]byte("[ Terminated ]\n"))
	c.Pipes.out.Flush()

	c.Pipes.fdin.Close()
	c.Pipes.fdout.Close()
	os.Remove(pipePath(c.Index, "i"))
	os.Remove(pipePath(c.Index, "o"))
	t := []*Connection{}
	for i := range cm.active {
		if cm.active[i].Index == c.Index {
			continue
		}
		t = append(t, cm.active[i])
	}

	cm.active = t
	fmt.Println("[!] Endpoint", c.Index, "stopped")
}

// Construct a new connection manager
func NewConnectionManager() *ConnectionManager {
	c := new(ConnectionManager)
	c.cid = 1
	return c
}

// Generate a path to a given pipe
func pipePath(index int, suffix string) string {
	rundir := os.Getenv("HCROOT") + "/run"
	return fmt.Sprintf("%s/t%d%s", rundir, index, suffix)
}
