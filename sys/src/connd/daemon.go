package main

import (
	"fmt"
	"net"
	"os"
)

type Daemon struct {
	uds net.Listener
	cm  *ConnectionManager
}

func NewDaemon(cm *ConnectionManager) (*Daemon, error) {

	path := os.Getenv("HCROOT") + "/run/connd"
	err := os.Remove(path)

	s, err := net.Listen("unix", path)
	if err != nil {
		return nil, err
	}

	if err = os.Chmod(path, 0666); err != nil {
		return nil, err
	}

	d := new(Daemon)
	d.uds = s
	d.cm = cm
	return d, nil
}

func (d *Daemon) Start() {
	fmt.Println("[+] Daemon Started")
	for {
		c, err := d.uds.Accept()
		if err != nil {
			fmt.Fprintf(os.Stderr, "[-] Error accepting connection:", err)
			return
		}

		go d.process(c)
	}
}

func (d *Daemon) process(c net.Conn) {
	buf := make([]byte, 1024)
	ah := MakeActionHandler(d.cm)
	n, err := c.Read(buf)
	if err != nil {
		c.Close()
	}

	if n > 0 {
		line := string(buf[:n-1])
		c.Write([]byte(ah.Run(line) + "\n"))
	}
	c.Close()
}
