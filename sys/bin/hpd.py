#!/usr/bin/python3.5
from http.server import BaseHTTPRequestHandler, HTTPServer
import os, argparse

HCROOT = os.environ['HCROOT']
DISP = HCROOT + '/share/disp'

class ReqHandler(BaseHTTPRequestHandler):
  def do_GET(self):
    self.dispatch(self.path)
    return

  def dispatch(self, path):
    global DISP
    p = DISP + path
    if not os.path.isfile(p):
      self.send_response(404)
      self.send_header('Content-Type','text/plain')
      self.end_headers()
      self.wfile.write(bytes("", "utf8"))
      return

    self.send_response(200)
    f = open(p, "rb")
    
    if path.split('/')[0] == 'bin':
      self.send_header('Content-Type','application/octet-stream')
    else:
      self.send_header('Content-Type','text/plain')
    self.end_headers()
    self.wfile.write(f.read())
    return

print("<harmonic> portal dispatch")

parser = argparse.ArgumentParser(description='Small dispatch server for pulling files through portal or for RFIs')

parser.add_argument('-p','--port', metavar='PORT', action="store", type=int, default=8080,
                    help='The port to run the server on (default: 8080)')

args = parser.parse_args()


httpd = HTTPServer(('0.0.0.0',args.port), ReqHandler)
httpd.serve_forever()
