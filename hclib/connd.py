import socket, os, traceback

class Connd:
    def __init__(self, udspath):
        self.udspath = udspath
        self.uds = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    def start(self, cmdline):
        try:
            self.uds.connect(self.udspath)
            rq = "start " + cmdline + "\n"
            self.uds.send(rq.encode())
            response = self.uds.recv(128)
            self.uds.close()
            return int(response)
        except:
            print("Error: Exception caught when writing to 'connd' socket -- is it running?")
            return -1

    def stop(self, eid):
        try:
            self.uds.connect(self.udspath)
            rq = "stop " + eid +"\n"
            self.uds.send(rq.encode())
            response = self.uds.recv(128)
            self.uds.close()
            return 0
        except Exception:
            print("Error: Exception caught when writing to 'connd' socket -- is it running?")
            traceback.print_exc()
            return -1

    def reset(self):
        try:
            self.uds.connect(self.udspath)
            rq = "reset\n"
            self.uds.send(rq.encode())
            response = self.uds.recv(128)
            self.uds.close()
            return 0
        except Exception:
            print("Error: Exception caught when writing to 'connd' socket -- is it running?")
            traceback.print_exc()
            return -1

    def list(self):
        try:
            self.uds.connect(self.udspath)
            rq = "list\n"
            self.uds.send(rq.encode())
            response = self.uds.recv(128)
            self.uds.close()
            return response
        except:
            print("Error: Exception caught when writing to 'connd' socket -- is it running?")
            return -1

def endpoint_in(eid):
    HCROOT = os.environ['HCROOT']
    return "{}/run/t{}i".format(HCROOT, eid)

def endpoint_out(eid):
    HCROOT = os.environ['HCROOT']
    return "{}/run/t{}o".format(HCROOT, eid)

def connd_path():
    HCROOT = os.environ['HCROOT']
    return HCROOT+"/run/connd"

def endpoint_open_pipes(eid):
    fdin = open(endpoint_in(eid), "wb")
    fdout = open(endpoint_out(eid), "rb")
    return (fdin, fdout)
