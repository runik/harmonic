from .poll import process
from .connd import Connd
from .rc import *
import fcntl,os,time,sys,hashlib,datetime

def gen_delim():
    return hashlib.sha224(datetime.datetime.now().strftime("%Y%m%d%H%M%S").encode()).hexdigest()
    
    
def fd_nonblocking(fd):
    f = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, f | os.O_NONBLOCK)


def args_to_dictionary(args):
    d = {}
    parts = args.split(" ")

    for p in parts:
        kv = p.split("=")

        # set any unnamed variables to _V
        # which means a script can have one
        # variable that does not have a label
        # X=.
        # For example: !run mac pty
        # Where pty is the macro assigned to _V
        if len(kv) == 1:
            if '_V' in d:
                d['_V'] = d['_V'] + " " + kv[0]
            else:
                d['_V'] = kv[0]
            continue

        d[kv[0]] = kv[1]

    return d

def merge_args(env, args):
    for k in env:
        if not k in args:
            args[k] = env[k]

def merge_cmdline(pos):
    env = load_env()
    args = {}
    argc = len(sys.argv)
    for k in env['global']:
        if not k in pos:
            args[k] = env['global'][k]
        elif argc > pos[k]:
            args[k] = sys.argv[pos[k]]
        else:
            args[k] = env['global'][k]

    for k in pos:
        if not k in env['global'] and argc > pos[k]:
            args[k] = sys.argv[pos[k]]

    return args
    
def fd_cmd(cmd, fdin):
    o = cmd + "\n"
    fdin.write(str.encode(o))
    fdin.flush()
    time.sleep(0.3)

def fd_readexp(fdout):
    out = ""
    while True:
        try:
            out += os.read(fdout.fileno(), 1024).decode()
        except OSError:
            break
    return out

def fd_readkey(keyword, fdout, output = True):
    out = ""
    while True:
        try:
                out += os.read(fdout.fileno(), 1024).decode()
                pos = out.find("\n"+keyword)
                if pos != -1:
                    out = out[0:pos]
                    if output:
                        sys.stdout.write(out)
                        sys.stdout.flush()
                    break

                if output:
                    sys.stdout.write(out)
                    sys.stdout.flush()

        except:
            continue

    return out
