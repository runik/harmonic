import os, configparser
def load_env():
    HCROOT = os.environ['HCROOT']
    cfg = configparser.ConfigParser()
    cfg.optionxform = str
    cfg['global'] = { }
    cfg.read(HCROOT + "/.hcrc")

    # replace global config vars with environment vars
    for k in os.environ.keys():
        if k[:4] == 'HCV_':
            cfg['global'][k[4:]] = os.environ[k]

    return cfg
