import readline,os,subprocess,sys,select,fcntl
from threading  import Thread

class Process:
    def __init__(self,cmd):
        self.p = subprocess.Popen(cmd, stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
        print("spawned pid: {}".format(self.p.pid))
        f = fcntl.fcntl(self.p.stdout, fcntl.F_GETFL)
        fcntl.fcntl(self.p.stdout, fcntl.F_SETFL, f | os.O_NONBLOCK)
        self.t = Process._handler(self.p.stdout)
        self.t.deamon = True
        self.t.start()

    def focus(self):
        while True:
          line = input()

          if self.p.poll() != None:
            self.t.signal_end()
            self.t.join()
            self.p.stdin.close()
            return
        
          line += '\n'
          self.p.stdin.write(line.encode())
          self.p.stdin.flush()

    class _handler(Thread):
        def __init__(self, fpout):
            Thread.__init__(self)
            self.fpout = fpout
            self.spawned = True

        def run(self):
            poller = select.poll()
            poller.register(self.fpout, select.POLLIN)
            while self.spawned:
                r = poller.poll(100)
                if r:
                    sys.stdout.write(self.fpout.read(2048).decode())
                    sys.stdout.flush()
            poller.unregister(self.fpout)

        def signal_end(self):
            self.spawned = False

