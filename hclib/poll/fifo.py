import os,subprocess,sys,select,fcntl,signal
from threading  import Thread

class Fifo:
    def __init__(self,fdin, fdout, ffocus = None, foutput = None):
        self.fdin = fdin
        self.fdout = fdout
        self.ffocus = ffocus
        self.foutput = foutput
        f = fcntl.fcntl(self.fdout, fcntl.F_GETFL)
        fcntl.fcntl(self.fdout, fcntl.F_SETFL, f | os.O_NONBLOCK)
        if self.ffocus == None:
            self.ffocus = self.dfltfocus
            
        self.signal_start()

    def signal_start(self):
        self.t = Fifo._handler(self.fdout, self.foutput)
        self.t.deamon = True
        self.t.start()

    def focus(self, keywords = {}):
        try:
            self.ffocus(keywords)
        except:
            self.signal_end()
        
    def dfltfocus(self, keywords = {}):
        while True:
          ch = ''
          line = ''
          while ch != '\n':
            line += ch
            ch = sys.stdin.read(1)
          parts = line.split(" ")
          if parts[0] in keywords:
            self.signal_end()
            keywords[parts[0]](parts[1:])
            self.signal_start()
          else:
            line += '\n'
            self.fdin.write(str.encode(line))
            self.fdin.flush()

    def signal_end(self):
        self.t.signal_end()
        self.t.join()

    class _handler(Thread):
        def __init__(self, fdout, foutput):
            Thread.__init__(self)
            self.fdout = fdout
            self.foutput = foutput
            if self.foutput == None:
                self.foutput = self.dfltwrite

            self.spawned = True

        def dfltwrite(self, out):
            sys.stdout.write(out)
            sys.stdout.flush()

        def run(self):
            poller = select.poll()
            poller.register(self.fdout, select.POLLIN)
            while self.spawned:
                r = poller.poll(100)
                if r:
                    out = self.fdout.read(2048).decode("utf-8", "backslashreplace")
                    self.foutput(out)
            poller.unregister(self.fdout)
        def signal_end(self):
            self.spawned = False
