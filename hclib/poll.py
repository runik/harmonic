import readline,os,subprocess,sys,select
from threading  import Thread

class process:
    def __init__(cmd):
        self.p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        self.t = process._handler(p.stdout)
        self.spawned = True
        self.t.start()

    def focus(self):
        while True:
          line = raw_input()

          if p.poll() != None:
            self.spawned = False
            t.join()
            p.stdin.close()
            return
          p.stdin.write(line+"\n")

    class _handler(threading.Thread):
        def __init__(self, fpout):
            threading.Thread.__init__(self)
            self.fpout = fpout
            
        def run(fpout):
            poller = select.poll()
            poller.register(self.fpout, select.POLLIN)
            while self.spawned:
                r = poller.poll(0)
                if r:
                sys.stdout.write(self.fpout.read(1))
                sys.stdout.flush()
