#!/usr/bin/python3
import os, select, sys, signal,readline, traceback
from threading import Thread

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
import hclib
ENV = hclib.rc.load_env()



argc = len(sys.argv)

if argc < 3 or sys.argv[1] == '-h':
    print(
    "usage: hscr.py [-h] EID SCR [ARG ...]\n\n"
    "Run a hookable script against an endpoint. The scripts are"
    "found in $HCROOT/share/script\n\n"
    "positional arguments:\n"
    "  EID        The endpoint ID\n"
    "  SCR        The script to run\n"
    "  ARG        A list of arguments to run\n"
    "\n"
    "optional arguments:\n"
    "  -h         Show this help\n"
    )
    exit(1)

eid = sys.argv[1]
script = sys.argv[2]

args = " ".join(sys.argv[3:])

cmdline = HCROOT+"/share/script/{}.py {} {}".format(script, eid, args)
os.system(cmdline)
