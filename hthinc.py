#!/usr/bin/python3
import argparse, os, select, sys, signal,readline, traceback
from threading import Thread

print("<harmonic> thinc")
HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
sys.path.append(HCROOT + "/share/script/.autocomp")
import hclib
ENV = hclib.rc.load_env()

parser = argparse.ArgumentParser(description='Thin software client for interfacing with connd endpoints')

parser.add_argument('eid', metavar='ID', action="store", type=int,
                    help='The endpoint ID of the connection')

args = parser.parse_args()

fd = hclib.connd.endpoint_open_pipes(args.eid)
endpoint = hclib.poll.Fifo(fd[0], fd[1])
acmods = {}

def signal_handler(signal, frame):
  endpoint.signal_end()
  exit()
 
signal.signal(signal.SIGINT, signal_handler)

def kw_help(args = []):
    print(
        "<harmonic> thinc\n\n"
        "A thin software client for interfacing with connd endpoints. It can be used against any endpoint process (nc, hwsh).\n\n"
        "inbuilt commands:\n"
        "!run SCR      Run a hookable script\n"
        "!connd ARGS   Interact with connd"
        "!help         Show this message\n\n"

        "hookable scripts:\n"
        "sp            Attempt to spawn an interactive shell\n"
        "  LHOST=      The IP Address of the local endpoint\n"
        "  LPORT=      The port for the local endpoint to listen\n"
        "  DRIVER=     Don't enumerate and use this driver\n"
        "mac           Run a macro; the macro is given immediately after command !run mac MAC\n"
        "  _V          Unlabeled variable specifying macro\n"
        "lpc           Run Linux Priv Checker\n"
        "pull          Pull a file from a dispatch server (wget macro, linux)\n"
        "  FILE=       The file to dispatch\n"
        "  DHOST=      Portal dispatch host\n"
        "  DPORT=      Portal dispatch port\n"
        "wpinit        Generate a portal script (windows)\n"
        "vbpull        Pull file using VB portal (windows)\n"
        "  FILE=       The file to pull\n"
        "  DHOST=      Address of dispatch server\n"
        "  DPORT=      Port of dispatch server\n"
        "pspull        Pull file using PowerShell portal (windows)\n"
        "  FILE=       The file to pull\n"
        "  DHOST=      Address of dispatch server\n"
        "  DPORT=      Port of dispatch server\n"
        "\n"
    )


def kw_run(args):
    global fd
    global ENV
    global endpoint
    
    if len(args) == 0:
        print("No script specified")
        return
    script = args[0]
    sys.path.append(HCROOT + "/share/script")
    args = hclib.args_to_dictionary(" ".join(args[1:]))
    hclib.merge_args(ENV['global'], args)
  
    s = __import__(script)
    endpoint.signal_end()
    s.hook(fd[0], fd[1], args)
    del s
    
    

def kw_connd(args):
    cnd = hclib.connd.Connd(hclib.connd.connd_path())
    if len(args) < 2:
        print("No endpoint or parameters specified")
        return
    if args[0] == 'start':
      if len(args) < 3:
        print("No endpoint parameter specified")
        return
      if args[1] != 'nc' and args[1] != 'wsh':
        print("No valid endpoint specified")
        return
      sys.stdout.write("[+] Starting endpoint... ")
      sys.stdout.flush();
      eid = -1
      if args[1] == 'nc':
          eid = cnd.start("nc -lp " + args[2])
      elif args[1] == 'wsh':
          eid = cnd.start("hwsh.py " + args[2])
      print( "OK ({})".format(eid))
    elif args[0] == 'stop':
      if cnd.stop(args[1]) == 0:
        print("[+] Stopped endpoint {}".format(args[1]))
      else:
        print("[-] Error closing endpoint")
    else:
      print("[-] Bad command (start|stop)")
      
        
        

keywords = {
    '!help' : kw_help,
    '!!' : kw_run,
    '!connd' : kw_connd,
}
endpoint.focus(keywords)
