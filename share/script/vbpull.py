#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
import hclib

def hook(fdin, fdout, args):

    if not 'FILE' in args:
        print("Unspecified FILE value")
        return

    parts = args['FILE'].split('/')
    fname = parts[len(parts)-1]
    cmd = "cscript portal.vbs " + args['FILE'] +" & echo PENDP"
    print(cmd)
    print("[+] Portal --> () --> " + fname)
    hclib.fd_cmd(cmd, fdin)
    response = hclib.fd_readkey('\nPENDP',fdout, output=False)
    if response.find('200 OK') != -1:
        print("[+] Pull successful")
    elif response.find("Can not find script file"):
        print("[-] Cannot find portal script in directory (have you run wpinit?)")
    else:
        print("[-] Error retrieving file -- not found")
    return

def print_help():
    print(
        "usage: " + sys.argv[0] + " EID FILE\n\n"

        "Pull a file onto the windows system from dispatch using a portal\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    FILE       The file to pull\n"
    )
    
def main():
    print("<harmonic> Portal")
    argc = len(sys.argv)
    if argc < 3 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    args = hclib.merge_cmdline({ 'FILE':2})
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
