#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
import hclib

def hook(fdin, fdout, args, key = ''):
    # use _V as macro specifier
    if not '_V' in args:
        print("Unspecified Macro")
        return

    rc = hclib.rc.load_env()
    if not 'macro' in rc:
        print("[-] Macro section does not exist in resource file")
        return
    if args['_V'] == "--list":
        for mac in rc['macro']:
            print(mac)
        return

    if key != '':
        key = ";echo "+key
 
    if args['_V'] in rc['macro']:
        hclib.fd_cmd(rc['macro'][args['_V']] + key, fdin)
    else:
        print("[-] Macro does not exist")
    

def print_help():
    print(
        "usage: " + sys.argv[0] + " EID M\n\n"

        "Run a macro on the end point. Macros are defined in $HCROOT/.hcrc [macro]\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    M          The macro to run\n"
    )
    
def main():
    print("<harmonic> Macro")
    argc = len(sys.argv)
    if argc < 3 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    key = hclib.gen_delim()[0:16]
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    hclib.fd_nonblocking(fd[1])
    args = hclib.merge_cmdline({ '_V':2 })
    hook(fd[0], fd[1], args, key)
    
    hclib.fd_readkey(key, fd[1])
    print("")

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
