#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
import hclib

def hook(fdin, fdout, args):

    if not '_V' in args or not args['_V']:
        print("Unspecified command value")
        return

    tmp = args['_V'].split("/")
    
    filename = tmp[len(tmp)-1]
    path = os.getcwd() + "/"+filename;
    rev = 0
    while os.path.isfile("{}_{}".format(path, rev)):
        rev += 1
    path = "{}_{}".format(path, rev)
    
    key = hclib.gen_delim()
    cmd = args['_V'] +";echo "+key
    
    hclib.fd_cmd(cmd, fdin)
    response = hclib.fd_readkey(key,fdout, output=False)
    print(response)

    fp = open(path, "w")
    fp.write(args['_V'] + "\n\n" + response)
    fp.close()
    print("[+] Saved to " + path)
    return

def print_help():
    print(
        "usage: " + sys.argv[0] + " EID CMD [ARGS...]\n\n"

        "Log the result of a command to a file with commands name\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    CMD        The command to run\n"
    )
    
def main():
    print("<harmonic> Log")
    argc = len(sys.argv)
    if argc < 3 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    args = hclib.merge_cmdline({})
    args['_V'] = " ".join(sys.argv[2:])
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
