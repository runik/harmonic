#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
import hclib

def hook(fdin, fdout, args):

    if not 'DHOST' in args:
        print("Unspecified DHOST value")
        return
    
    if not 'DPORT' in args:
        print("Unspecified DPORT value")
        return
    
    if not 'FILE' in args:
        print("Unspecified FILE value")
        return
    delim = hclib.gen_delim()
    parts = args['FILE'].split('/')
    fname = parts[len(parts)-1]
    cmd = "fetch -o '"+fname+"' http://" + args['DHOST'] + ":" + args['DPORT'] + "/" + args['FILE'] +" 2>&1; echo '"+delim+"'"
    print("[+] " + args['DHOST'] + " --> () --> " + fname)
    hclib.fd_cmd(cmd, fdin)
    response = hclib.fd_readkey(delim,fdout, output=False)
    if response.find(': Not Found') == -1:
        print("[+] Pull successful")
    else:
        print("[-] Error retrieving file -- not found")
    return

def print_help():
    print(
        "usage: " + sys.argv[0] + " EID FILE [DPORT] [DHOST]\n\n"

        "Pull a file onto the BSD system from dispatch\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    FILE       The file to pull\n"
        "    DPORT      The port of the portal dispatch\n"
        "    DHOST      The IP of the portal dispatch\n"
    )
    
def main():
    print("<harmonic> Portal")
    argc = len(sys.argv)
    if argc < 3 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    args = hclib.merge_cmdline({ 'FILE':2, 'DHOST':4, 'DPORT':3})
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
