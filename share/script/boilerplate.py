#!/usr/bin/python3.5

import sys, os, subprocess, time, signal

HCROOT = os.environ['HCROOT']
UDS = HCROOT + "/run/connd"
sys.path.append(HCROOT)
import hclib

def hook(fdin, fdout, args):
    print("<harmonic> Script")
    lhost = ""
    lport = 8080
    if 'LHOST' in args:
        lhost = args['LHOST']
    else:
        print("Unspecified LHOST value")
        return

    if 'LPORT' in args:
        lport = args['LPORT']



def print_help():
    print(
        "usage: " + sys.argv[0] + " EID LHOST LPORT\n\n"

        "Basic boilerplate of a script\n\n"

        "Arguments:\n"
        "    EID        The endpoint ID to interact with\n"
        "    LHOST      The IP of the portal dispatch\n"
        "    LPORT      The port of the portal dispatch\n"
    )
    
def main():
    argc = len(sys.argv)
    if argc < 4 or sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        exit(1)
    eid = sys.argv[1]
    fd = hclib.connd.endpoint_open_pipes(eid)
    args = { 'LHOST' : sys.argv[2], 'LPORT' : sys.argv[3] }
    hook(fd[0], fd[1], args)
     

if __name__ == "__main__":
    sys.exit(main())

def signal_handler(signal, frame):
  exit()

signal.signal(signal.SIGINT, signal_handler)
