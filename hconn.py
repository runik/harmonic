#!/usr/bin/python3
import os, sys

HCROOT = os.environ['HCROOT']
UDS = HCROOT + "/run/connd"
sys.path.append(HCROOT)
import hclib
def print_help():
    print(
        "usage: " + sys.argv[0] + " ACTION [ARGS ...]\n\n"

        "Tool for managing connd daemon and its endpoints\n\n"
        "Arguments:\n"
        "  ACTION                 The action connd should perform (start|stop|reset|list)\n"
        "  ARGS                   The arguments to pass to the action\n\n"
        "Actions:\n"
        "  start                  Start an endpoint using arguments\n"
        "  stop                   Stop an endpoint with given ID\n"
        "  reset                  Reset all endpoints and IDs\n"
        "  list                   List active endpoints\n\n"

        "Endpoints:\n"
        "  nc PORT                Start a nc listener on PORT\n"
        "  wsh URL [ARGS ...]     Start a wedge shell at given URL\n"
        "  with CMD [ARGS ...]    Start an arbitrary endpoint with CMD using ARGS\n"
    )

def endpoint_nc(c, port):
    print(c.start("cont nc -nlvp {}".format(port)))

def endpoint_hwsh(c, url, extra):
    extrastr = ''
    if extra:
        extrastr = " "
        extrastr += " ".join(extra)
    print(c.start("disc hsysws.py {}{}".format(url,extrastr)))

def endpoint_cmd(c, cmdline):
    print(c.start(cmdline))

argc = len(sys.argv)

if argc == 1 or (sys.argv[1] == '-h' or sys.argv[1] == '--help'):
        print_help()
        exit(0)


action = sys.argv[1]

c = hclib.connd.Connd(UDS)

if action == 'start':
    if argc < 4:
        print("Error: Unspecified endpoint and arguments")
        exit(1)
    endpoint = sys.argv[2]
    args = sys.argv[3:]

    if endpoint == 'nc':
        endpoint_nc(c, args[0])
    elif endpoint == 'wsh':
        endpoint_hwsh(c, args[0], args[1:])
    elif endpoint == 'cmd':
        endpoint_cmd(c, " ".join(args))
    else:
        print("Error: Unrecognised endpoint")
        exit(1)
        
elif action == 'stop':
    if argc < 3:
        print("Error: Unspecified endpoint ID")
        exit(1)
    eid = sys.argv[2]
    print(c.stop(eid))
elif action == 'reset':
    print(c.reset())
elif action == 'list':
    sys.stdout.write(c.list().decode())
else:
    print("Error: Unrecognised action `"+action+"` -- start | stop")
    exit(1)


