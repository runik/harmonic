#!/usr/bin/python

import SocketServer, sys, argparse

parser = argparse.ArgumentParser(description='<harmonic> dispatch -- listen and send piped stdin to client')

parser.add_argument('-P', '--port', dest='LPORT', metavar='LPORT', action="store", type=int, default=4455,
                    help='The port to listen on')

parser.add_argument('-H', '--host', dest='LHOST', metavar='LHOST', action="store", default="0.0.0.0",
                    help='The interface to listen to listen on')


args = parser.parse_args()


payload = sys.stdin.read()
size = len(payload)

class HdisHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		sys.stdout.write("Dispatching {} byte payload...".format(size))
		sys.stdout.flush()
		self.request.sendall(payload)
		sys.stdout.write("OK\n")
    

	



print("<harmonic> dispatch {} {}".format(args.LHOST, args.LPORT))
try:
  s = SocketServer.TCPServer((args.LHOST, args.LPORT), HdisHandler)
  s.handle_request()
except Exception, ex:
	print("Caught exception: {}".format(ex))

s.server_close()
