#!/usr/bin/python3
import os, select, sys, signal,readline, traceback
from threading import Thread

HCROOT = os.environ['HCROOT']
sys.path.append(HCROOT)
sys.path.append(HCROOT + "/share/script/.autocomp")
import hclib
ENV = hclib.rc.load_env()



argc = len(sys.argv)

if argc < 3 or sys.argv[1] == '-h':
    print(
    "usage: hcmd.py [-h] EID CMD [ARG ...]\n\n"
    "Run a command against an endpoint and dump to stdout\n\n"
    "positional arguments:\n"
    "  EID        The endpoint ID\n"
    "  CMD        The command to run\n"
    "  ARG        A list of arguments to run\n"
    "\n"
    "optional arguments:\n"
    "  -h         Show this help\n"
    )
    exit(1)

key = hclib.gen_delim()[0:16]
cmdline = " ".join(sys.argv[2:])
cmdline += ";echo "+key
fd = hclib.connd.endpoint_open_pipes(int(sys.argv[1]))
hclib.fd_cmd(cmdline, fd[0])
output = hclib.fd_readkey(key, fd[1], False)

parts = output.split("\n", 1)
if parts[0].strip() == cmdline:
    print(parts[1])
else:
    print(output)

fd[0].close()
fd[1].close()
